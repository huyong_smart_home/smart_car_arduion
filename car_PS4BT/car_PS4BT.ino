/*
 Example sketch for the PS4 Bluetooth library - developed by Kristian Lauszus
 For more information visit my blog: http://blog.tkjelectronics.dk/ or
 send me an e-mail:  kristianl@tkjelectronics.com
 */

#include <PS4BT.h>
#include <usbhub.h>

// Satisfy the IDE, which needs to see the include statment in the ino too.
#ifdef dobogusinclude
#include <spi4teensy3.h>
#endif
#include <SPI.h>
#include <AFMotor.h>
#include <Servo.h> 

Servo servo1;
AF_DCMotor motor(4);

USB Usb;
//USBHub Hub1(&Usb); // Some dongles have a hub inside
BTD Btd(&Usb); // You have to create the Bluetooth Dongle instance like so

/* You can create the instance of the PS4BT class in two ways */
// This will start an inquiry and then pair with the PS4 controller - you only have to do this once
// You will need to hold down the PS and Share button at the same time, the PS4 controller will then start to blink rapidly indicating that it is in pairing mode
//PS4BT PS4(&Btd, PAIR);

// After that you can simply create the instance like so and then press the PS button on the device
PS4BT PS4(&Btd);

bool printAngle, printTouch;
uint8_t oldL2Value, oldR2Value;

void setup() {
  Serial.begin(115200);
#if !defined(__MIPSEL__)
  while (!Serial); // Wait for serial port to connect - used on Leonardo, Teensy and other boards with built-in USB CDC serial connection
#endif
  if (Usb.Init() == -1) {
    Serial.print(F("\r\nOSC did not start"));
    while (1); // Halt
  }
  Serial.print(F("\r\nPS4 Bluetooth Library Started"));

  motor.setSpeed(255);
  motor.run(RELEASE);
  servo1.attach(9);
  
}

void faw(){
   motor.run(FORWARD);
}

void back(){
  motor.run(BACKWARD);
}
void stop(){
  motor.run(RELEASE);
}
void right(){
  //todo
}
void left(){
  //todo
}

void loop() {
  Usb.Task();

  if (PS4.connected()) {

    if (PS4.getAnalogHat(LeftHatX) > 137 || PS4.getAnalogHat(LeftHatX) < 117 || PS4.getAnalogHat(LeftHatY) > 137 || PS4.getAnalogHat(LeftHatY) < 117 || PS4.getAnalogHat(RightHatX) > 137 || PS4.getAnalogHat(RightHatX) < 117 || PS4.getAnalogHat(RightHatY) > 137 || PS4.getAnalogHat(RightHatY) < 117) {
      Serial.print(F("\r\nLeftHatX: "));
      Serial.print(PS4.getAnalogHat(LeftHatX));
      Serial.print(F("\tLeftHatY: "));
      Serial.print(PS4.getAnalogHat(LeftHatY));
      Serial.print(F("\tRightHatX: "));
      Serial.print(PS4.getAnalogHat(RightHatX));
      Serial.print(F("\tRightHatY: "));
      Serial.print(PS4.getAnalogHat(RightHatY));
    }
    if(PS4.getAnalogHat(LeftHatY) > 137){
       Serial.print(F("\r\nUp :"));
       Serial.print(PS4.getAnalogHat(RightHatY));
       faw();
    }
    else if(PS4.getAnalogHat(LeftHatY) < 117){
       Serial.print(F("\r\nDown :"));
       Serial.print(PS4.getAnalogHat(RightHatY));
       back();
    }else if(PS4.getAnalogHat(LeftHatX) > 137){
       Serial.print(F("\r\nRight :"));
       Serial.print(PS4.getAnalogHat(LeftHatX));
       right();
    }else{
       Serial.print(F("\r\n stop :"));
       Serial.print(PS4.getAnalogHat(RightHatY));
       stop();
    }

    if (PS4.getButtonClick(PS)) {
      Serial.print(F("\r\nPS"));
      PS4.disconnect();
    }
    else {
      if (PS4.getButtonClick(TRIANGLE)) {
        Serial.print(F("\r\nTraingle"));
//        PS4.setRumbleOn(RumbleLow);
      }
      if (PS4.getButtonClick(CIRCLE)) {
        Serial.print(F("\r\nCircle"));
//        PS4.setRumbleOn(RumbleHigh);
      }
      if (PS4.getButtonClick(CROSS)) {
        Serial.print(F("\r\nCross"));
//        PS4.setLedFlash(10, 10); // Set it to blink rapidly
      }
      if (PS4.getButtonClick(SQUARE)) {
        Serial.print(F("\r\nSquare"));
//        PS4.setLedFlash(0, 0); // Turn off blinking
      }

      if (PS4.getButtonClick(UP)) {
        Serial.print(F("\r\nUp"));
        faw();
      } if (PS4.getButtonClick(RIGHT)) {
        Serial.print(F("\r\nRight"));
//        PS4.setLed(Blue);
        right();
      } if (PS4.getButtonClick(DOWN)) {
        Serial.print(F("\r\nDown"));
//        PS4.setLed(Yellow);
        back();
      } if (PS4.getButtonClick(LEFT)) {
        Serial.print(F("\r\nLeft"));
//        PS4.setLed(Green);
      }
    }
  }
}
