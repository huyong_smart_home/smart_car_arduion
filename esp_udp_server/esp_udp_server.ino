#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

const char* ssid = "Xiaomi_5C2D";
const char* password = "82370288";

WiFiUDP Udp;
unsigned int localUdpPort = 30666;  // local port to listen on
char incomingPacket[18];  // buffer for incoming packets
char  replyPacket[] = "Hi there! Got the message :-)";  // a reply string to send back


void setup()
{
  
  Serial.begin(115200);
  Serial.println();

  Serial.printf("Connecting to %s ", ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println(" connected");

  Udp.begin(localUdpPort);
  Serial.printf("Now listening at IP %s, UDP port %d\n", WiFi.localIP().toString().c_str(), localUdpPort);
}


void process(){
//  Serial.println(incomingPacket);
  incomingPacket[17] = 's';
  Serial.write(incomingPacket);
}
void loop()
{
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {
    // receive incoming UDP packets
//    Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
    int len = Udp.read(incomingPacket, 17);
    if (len > 0)
    {
      incomingPacket[len] = 0;
    }
//    Serial.printf("UDP packet contents: %s\n", incomingPacket);
    process();
    // send back a reply, to the IP address and port we got the packet from
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(replyPacket);
    Udp.endPacket();
  }
}
